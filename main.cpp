#include <QApplication>
#include "window.h"
//#include "user_def.h"

//----------------------------------------------------------------------------
//
// the user's time-consuming function
//
//----------------------------------------------------------------------------

double my_func(void)
{
// time-consuming code
   double sum(1);
   for (int i=0; i<100000; i++)
   {
      for (int j=1; j<100000; j++)
      {
         sum += i / j;
      }
   }
   return sum;
}

//----------------------------------------------------------------------------
//
// main program
//
//----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  Window window;
  window.show();

  return a.exec();
}
