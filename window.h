#ifndef WINDOW_H
#define WINDOW_H

#include <QtWidgets/QWidget>
#include <QPushButton>
#include <QLabel>
#include <QThread>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include <QTimer>

double my_func(void);

class Window : public QWidget
{
  Q_OBJECT
public:
  explicit Window(QWidget *parent = 0);
private:
   QPushButton *runButton;
signals:

public slots:
   void run_thread();
};

#endif // WINDOW_H
