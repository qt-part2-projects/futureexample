#include "window.h"

Window::Window(QWidget *parent) : QWidget(parent)
{
  setWindowTitle("Qt example 03");
  runButton = new QPushButton("Run the function...");
  connect(runButton, SIGNAL(clicked()),this, SLOT(run_thread()));

  QHBoxLayout *layout = new QHBoxLayout;
  layout->addWidget(runButton);
  setLayout(layout);
}
//-----
// slot
//-----

void Window::run_thread()
{
   qDebug() << my_func ();
}

